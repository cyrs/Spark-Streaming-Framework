/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import bigdata.java.framework.spark.util.es5.ElasticSearchPool;
import bigdata.java.framework.spark.util.es5.ElasticSearchPoolConfig;
import bigdata.java.framework.spark.util.es5.HostAndPort;
import com.typesafe.config.Config;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.cluster.state.ClusterStateResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.SearchHit;
import java.util.*;
import java.util.stream.Collectors;

/**
 *ElasticSearch5工具类
 * @deprecated 已废弃，建议使用bigdata.java.framework.spark.util.client.ESClient
 */
@Deprecated
public class ElasticSearch5Util {
    private static ElasticSearch5Util instance=null;
    public static ElasticSearch5Util getInstance(){
        if(instance == null){
            synchronized(ElasticSearch5Util.class){
                if(instance==null){
                    instance = new ElasticSearch5Util();
                }
            }
        }
        return instance;
    }

    private ElasticSearchPool elasticSearchPool = null;

    private ElasticSearch5Util()
    {
        System.setProperty("es.set.netty.runtime.available.processors","false");
        Set<HostAndPort> nodes = new HashSet<HostAndPort>();
        Config conf = ConfigUtil.getConfig();
        String nd =conf.getString("es.nodes");
        String[] split = nd.split(",");
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            String[] serverAndPort = s.split(":");
            String ip = serverAndPort[0];
            Integer port = Integer.parseInt(serverAndPort[1]);
            nodes.add(new HostAndPort(ip,port,"http"));
        }
        ElasticSearchPoolConfig config = new ElasticSearchPoolConfig();
        config.setConnectTimeMillis(8000);
        config.setMaxWaitMillis(10000);
        config.setMaxTotal(10);
        config.setMinIdle(1);
        config.setClusterName("elasticsearch");
        config.setNodes(nodes);
        elasticSearchPool = new ElasticSearchPool(config);
    }

    /**
     * 获取连接池对象
     */
    public ElasticSearchPool getPool()
    {
        return elasticSearchPool;
    }

    /**
     * 查看集群信息
     */
    public static void getClusterInfo() {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try
        {
            List<DiscoveryNode> nodes = resource.connectedNodes();
            for (DiscoveryNode node : nodes) {
                System.out.println("HostId:"+node.getHostAddress()+" hostName:"+node.getHostName()+" Address:"+node.getAddress());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            pool.returnResource(resource);
        }
    }

    /**
     * 验证索引是否存在
     * @param index 索引名
     */
    public static boolean indexExist(String index) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        IndicesExistsResponse inExistsResponse=null;
        try
        {
            IndicesExistsRequest inExistsRequest = new IndicesExistsRequest(index);
            inExistsResponse = resource.admin().indices()
                    .exists(inExistsRequest).actionGet();
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return inExistsResponse.isExists();
    }
    /**
     * 插入数据
     * @param index 索引名
     * @param type 类型
     * @param json json数据
     */
    public static Long insertData(String index, String type, String json) {
        return insertData(index,type,null,json);
    }

    /**
     * 插入文档，参数为json字符串
     * @param index 索引名
     * @param type 类型
     * @param _id 数据id
     * @param json 数据
     */
    public static Long insertData(String index, String type, String _id, String json) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        IndexResponse response = null;
        try {
            if(StringUtils.isBlank(_id))
            {
                response = resource.prepareIndex(index, type)
                        .setSource(json)
                        .get();
            }
            else
            {
                response = resource.prepareIndex(index, type).setId(_id)
                        .setSource(json)
                        .get();
            }
            pool.returnResource(resource);

        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return response.getVersion();
    }

    /**
     * 更新数据
     * @param index 索引名
     * @param type  类型
     * @param _id   数据id
     * @param json  数据
     */
    public static void updateData(String index, String type, String _id, String json){
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try {
            UpdateRequest updateRequest = new UpdateRequest(index, type, _id).doc(json);
                    resource.update(updateRequest).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            pool.returnResource(resource);
        }
    }

    /**
     * 删除指定文档数据
     * @param index 索引名
     * @param type 类型
     * @param _id 数据id
     */
    public static void deleteData(String index, String type, String _id) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try {
            DeleteResponse response = resource.prepareDelete(index, type, _id).get();
//            System.out.println(response.isFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            pool.returnResource(resource);
        }
    }

    /**
     * 根据查询进行删除
     * @param index 索引名
     * @param filter 过滤条件
     */
    public static void delete_by_query(String index, QueryBuilder filter)
    {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try
        {
//            BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
//            queryBuilder.must(QueryBuilders.termQuery("groupID","42023"));
            BulkByScrollResponse bulkByScrollResponse = DeleteByQueryAction.INSTANCE.newRequestBuilder(resource)
                    .filter(filter)
                    .source(index).get();
            long deleted = bulkByScrollResponse.getDeleted();
            System.out.println(deleted);
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            pool.returnResource(resource);
        }
    }

    /**
     * 根据id进行批量删除文档
     * @param index 索引名
     * @param type 类型
     * @param _ids ids
     */
    public static void deleteBatchData(String index, String type, List<String> _ids)
    {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try
        {
            BulkRequestBuilder builder = resource.prepareBulk();
            for (int i = 0; i < _ids.size(); i++) {
                String _id = _ids.get(i);
                builder.add(resource.prepareDelete(index,type,_id));
            }
            BulkResponse bulkItemResponses = builder.get();
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
    }

    /**
     * 批量删除索引库
     * @param list 索引库list
     */
    public static void deleteIndex(List<String> list)
    {
        if(list==null)return;
        for (int i = 0; i < list.size(); i++) {
            deleteIndex(list.get(i));
        }
    }

    /**
     * 删除索引库
     * @param index 索引名
     */
    public static void deleteIndex(String index) {
        if(StringUtils.isBlank(index))
        {
            return;
        }
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try {
            if (indexExist(index)) {
                DeleteIndexResponse dResponse = resource.admin().indices().prepareDelete(index)
                        .execute().actionGet();
                if (!dResponse.isAcknowledged()) {
                    throw new RuntimeException("failed to delete index " + index + "!");
//                    System.out.println("failed to delete index " + index + "!");
//                    logger.info("failed to delete index " + index + "!");
                }else {
                    pool.returnResource(resource);
                    System.out.println("delete index " + index + " successfully!");
//                    logger.info("delete index " + index + " successfully!");
                }
            } else {
                throw new RuntimeException("delete index " + index + " not exists!");
//                System.out.println("the index " + index + " not exists!");
//                logger.error("the index " + index + " not exists!");
            }
        } catch (Exception e) {
            pool.returnResource(resource);
            e.printStackTrace();
        }
    }

    /**
     * 删除索引类型表所有数据，批量删除
     * @param index 索引名
     * @param type 类型
     */
    public static void deleteIndexTypeAllData(String index, String type) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        try{
            SearchResponse response = resource.prepareSearch(index).setTypes(type)
                    .setQuery(QueryBuilders.matchAllQuery()).setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setScroll(new TimeValue(60000)).setSize(10000).setExplain(false).execute().actionGet();
            BulkRequestBuilder bulkRequest = resource.prepareBulk();
            while (true)
            {
                SearchHit[] hitArray = response.getHits().getHits();
                SearchHit hit = null;
                for (int i = 0, len = hitArray.length; i < len; i++) {
                    hit = hitArray[i];
                    DeleteRequestBuilder request = resource.prepareDelete(index, type, hit.getId());
                    bulkRequest.add(request);
                }
                BulkResponse bulkResponse = bulkRequest.execute().actionGet();
                if (bulkResponse.hasFailures()) {
//                logger.error(bulkResponse.buildFailureMessage());
                    throw new RuntimeException(bulkResponse.buildFailureMessage());
                }
                if (hitArray.length == 0) break;
                response = resource.prepareSearchScroll(response.getScrollId())
                        .setScroll(new TimeValue(60000)).execute().actionGet();

                pool.returnResource(resource);
            }
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
    }

    /**
     * 批量插入文档数据
     * @param index 索引名
     * @param type 类型
     * @param jsonList 批量数据json格式
     */
    public static void bulkInsertData(String index, String type, List<String> jsonList) {

        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        BulkResponse bulkResponse =null;
        try
        {
            BulkRequestBuilder bulkRequest = resource.prepareBulk();
            jsonList.forEach(item -> {
                bulkRequest.add(resource.prepareIndex(index, type).setSource(item));
            });
            bulkResponse = bulkRequest.get();
            if(bulkResponse.hasFailures()) {
                throw new RuntimeException(bulkResponse.buildFailureMessage());
            }
            pool.returnResource(resource);
        }
        catch ( Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
    }

    /**
     * 批量插入文档数据
     * @param index 索引名
     * @param type 类型
     * @param jsonList 批量数据json格式
     */
    public static void bulkInsertDataForId(String index, String type, List<String[]> jsonList) {

        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        BulkResponse bulkResponse =null;
        try
        {
            BulkRequestBuilder bulkRequest = resource.prepareBulk();
            jsonList.forEach(item -> {
                String _id = item[0];
                String json = item[1];
                bulkRequest.add(resource.prepareIndex(index, type,_id).setSource(json));
            });
            bulkResponse = bulkRequest.get();
            if(bulkResponse.hasFailures()) {
                throw new RuntimeException(bulkResponse.buildFailureMessage());
            }
            pool.returnResource(resource);
        }
        catch ( Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
    }


    /**
     * 根据id获取指定文档
     * @param index 索引名
     * @param type 类型
     * @param id id
     * @return 返回json
     */
    public static String getDocumentByIdAsString(String index, String type, String id) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        String json ="";
        try
        {
            // 搜索数据
            GetResponse response = resource.prepareGet(index, type, id)
//                .setOperationThreaded(false)    // 线程安全
                    .get();
            if(response.isExists())
            {
                json =response.getSourceAsString();
            }
//            System.out.println(response.isExists());  // 查询结果是否存在
//            System.out.println("***********"+response.getSourceAsString());//获取文档信息
//        System.out.println(response.toString());//获取详细信息
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 根据多个id批量获取指定文档信息
     * @param index 索引名
     * @param type 类型
     * @param ids ids
     * @return 返回json list
     */
    public static List<String> getMultiDocumentByIdAsString(String index, String type, String... ids) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        List<String> list = new ArrayList<>();
        try
        {
            MultiGetResponse responses = resource.prepareMultiGet()
                    .add(index,type,ids)
                    .get();

            for (MultiGetItemResponse itemResponses : responses)
            {
                GetResponse resp = itemResponses.getResponse();
                if(resp.isExists())
                {
                    list.add(resp.getSourceAsString());
                }
            }
//            System.out.println(response.isExists());  // 查询结果是否存在
//            System.out.println("***********"+response.getSourceAsString());//获取文档信息
//        System.out.println(response.toString());//获取详细信息
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 根据多个id批量获取指定文档信息
     * @param index 索引名
     * @param type 类型
     * @param ids ids
     * @return 返回map list
     */
    public static List<Map<String,Object>> getMultiDocumentByIdAsMap(String index, String type, String... ids) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        List<Map<String,Object>> list = new ArrayList<>();
        try
        {
            MultiGetResponse responses = resource.prepareMultiGet()
                    .add(index,type,ids)
                    .get();

            for (MultiGetItemResponse itemResponses : responses)
            {
                GetResponse resp = itemResponses.getResponse();
                if(resp.isExists())
                {
                    list.add(resp.getSourceAsMap());
                }
            }
//            System.out.println(response.isExists());  // 查询结果是否存在
//            System.out.println("***********"+response.getSourceAsString());//获取文档信息
//        System.out.println(response.toString());//获取详细信息
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 根据id获取指定文档信息
     * @param index 索引名
     * @param type 类型
     * @param id id
     * @return 返回map
     */
    public static Map<String,Object> getDocumentByIdAsMap(String index, String type, String id) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        Map<String,Object> map = new HashMap<>();
        try
        {
            // 搜索数据
            GetResponse response = resource.prepareGet(index, type, id)
//                .setOperationThreaded(false)    // 线程安全
                    .get();
            if(response.isExists())
            {
                map =response.getSourceAsMap();
            }
//            System.out.println(response.isExists());  // 查询结果是否存在
//            System.out.println("***********"+response.getSourceAsString());//获取文档信息
//        System.out.println(response.toString());//获取详细信息
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return map;
    }


    /**
     * 根据索引获取前5条文档
     * @param index 索引名
     * @return map list
     */
    public static List<Map<String, Object>> getDocuments(String index) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        List<Map<String, Object>> mapList = new ArrayList<>();
        try
        {
            // 搜索数据
            SearchResponse response = resource.prepareSearch(index)
//    			.setTypes("type1","type2"); //设置过滤type
//    			.setTypes(SearchType.DFS_QUERY_THEN_FETCH)  精确查询
//    			.setQuery(QueryBuilders.matchQuery(term, queryString));
//    			.setFrom(0) //设置查询数据的位置,分页用
//    			.setSize(60) //设置查询结果集的最大条数
//    			.setExplain(true) //设置是否按查询匹配度排序
                    .get(); //最后就是返回搜索响应信息
            System.out.println("共匹配到:"+response.getHits().getTotalHits()+"条记录!");
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> source = hit.getSource();
                mapList.add(source);
            }
            System.out.println(response.getTotalShards());//总条数
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return mapList;
    }

    /**
     * 获取指定索引库下指定type所有文档信息
     * @param index 索引名
     * @param type 类型
     * @return map list
     */
    public static List<Map<String, Object>> getDocuments(String index, String type) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        List<Map<String, Object>> mapList = new ArrayList<>();
        try
        {
            SearchResponse response = resource.prepareSearch(index).setTypes(type).get();
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> source = hit.getSource();
                mapList.add(source);
            }
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            pool.returnResource(resource);
        }
        return mapList;
    }

    /**
     * 根据索引前缀查找不符合noContainsIndex规则的索引名称
     * @param prefix 索引前缀
     * @param noContainsIndex 排除的索引
     * @return 返回匹配的索引名
     */
    public static List<String> searchIndexNoContainsIndex(String prefix,String noContainsIndex)
    {
        String index = prefix + noContainsIndex;
        List<String> list = searchIndex(prefix);
        List<String> collect = list.stream().filter(i -> !i.toLowerCase().equals(index.toLowerCase())).collect(Collectors.toList());
        return collect;
    }

    /**
     * 查找es中索引名称
     * @param prefix 索引名称
     * @return 匹配上的索引
     */
    public static List<String> searchIndex(String prefix)
    {
        List<String> allIndex = getAllIndex();
        List<String> collect = allIndex.stream().filter(i -> i.toLowerCase().startsWith(prefix.toLowerCase())).collect(Collectors.toList());
        return collect;
    }

    /**
     * 获取所有的索引库
     */
    public static List<String> getAllIndex(){
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        List<String> indexList = new ArrayList<>();
        try
        {
            ClusterStateResponse response = resource.admin().cluster().prepareState().execute().actionGet();
            //获取所有索引
            String[] indexs=response.getState().getMetaData().getConcreteAllIndices();
            if(indexs!=null && indexs.length > 0)
            {
                indexList = Arrays.asList(indexs);
            }
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return indexList;
    }

    /**
     * 新建索引库
     * @param indexName 索引名
     */
    public static boolean createIndex(String indexName) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        Boolean success=false;
        try {
            if (indexExist(indexName)) {
                throw new RuntimeException("The index " + indexName + " already exits!");
            } else {
                CreateIndexResponse cIndexResponse = resource.admin().indices()
                        .create(new CreateIndexRequest(indexName))
                        .actionGet();
                if (cIndexResponse.isAcknowledged()) {
                    success=true;
                } else {
                    throw new RuntimeException("Fail to create index "+indexName+" !");
                }
                pool.returnResource(resource);
            }
        } catch (Exception e) {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return success;
    }
    /**
     * 新建索引
     * @param indexName 索引名
     * @param typeName 类型
     * @param mapping mapping
     * @return 是否创建成功
     */
    public static boolean createIndex(String indexName,String typeName,XContentBuilder mapping) {
        ElasticSearchPool pool = ElasticSearch5Util.getInstance().getPool();
        TransportClient resource = pool.getResource();
        Boolean success=false;
        try
        {
//            XContentBuilder builder = XContentFactory
//                    .jsonBuilder()
//                    .startObject()
////                    .startObject(type)
//                    .startObject("properties")
//                    .startObject("name")
//                    .field("type","keyword")
//                    .field("store",true)
//                    .field("index",true)
//                    .endObject()
//                    .endObject()
////                    .endObject()
//                    .endObject();
            CreateIndexRequestBuilder crb = resource.admin().indices().prepareCreate(indexName);
            CreateIndexResponse createIndexResponse = crb.addMapping(typeName, mapping).execute().actionGet();
            if(createIndexResponse.isAcknowledged())
            {
                success = true;
            }
            pool.returnResource(resource);
        }
        catch (Exception e)
        {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return success;
    }

    /**
     * 新建索引库
     * @param index 索引名
     * @param type 类型
     */
    public static boolean createIndex(String index, String type) {
        ElasticSearchPool pool = getInstance().getPool();
        TransportClient resource = pool.getResource();
        Boolean success=false;
        try {
            IndexResponse indexResponse = resource.prepareIndex(index, type).setSource().get();
            success=true;
            pool.returnResource(resource);
        } catch (Exception e) {
            pool.returnResource(resource);
            e.printStackTrace();
        }
        return success;
    }
}
