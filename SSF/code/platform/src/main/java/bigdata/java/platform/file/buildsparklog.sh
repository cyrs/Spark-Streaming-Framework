#!/bin/bash
export HADOOP_USER_NAME=$2
LOG_DIR='/home/work/xtb/logs/'$1
LOG_DIR_ZIP=${LOG_DIR}.zip
#删除原来存在的文件
if [ -f "$LOG_DIR_ZIP" ];then
  rm ${LOG_DIR_ZIP}
fi
#通过yarn命令获取日志信息并存储到本地文件夹
yarn logs -applicationId $1 > ${LOG_DIR}.log
#压缩成zip文件
zip -j ${LOG_DIR}.zip ${LOG_DIR}.log
rm ${LOG_DIR}.log
