package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.TaskChart;
import bigdata.java.platform.service.TaskChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TaskChartServiceImpl implements TaskChartService {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<TaskChart> list(Integer taskId) {
        return null;
    }

    @Override
    public Boolean add(TaskChart taskChart) {
        return null;
    }

    @Override
    public Boolean add(List<String> list) {
        JdbcTemplate jdbcTemplate = namedParameterJdbcTemplate.getJdbcTemplate();
        jdbcTemplate.batchUpdate(list.toArray(new String[list.size()]));
        return true;
    }

    @Override
    public List<Map<String, Object>> getChart(Integer taskId)
    {
        String sql="select " +
                " (t2.consumeOffset-t1.consumeOffset) consume ,( t2.maxOffset-t1.maxOffset) max,t1.uptime" +
                " from " +
                " (select row_number() over (order by uptime asc ) row_num, sum(consumeOffset) consumeOffset,sum(maxOffset) maxOffset,uptime from TaskChart where taskId=:taskId group by uptime order by uptime asc )t1," +
                " (select row_number() over (order by uptime asc ) row_num, sum(consumeOffset) consumeOffset,sum(maxOffset) maxOffset,uptime from TaskChart where taskId=:taskId group by uptime order by uptime asc )t2" +
                " where t1.row_num + 1 =  t2.row_num";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        List<Map<String, Object>> list = namedParameterJdbcTemplate.queryForList(sql, parameters);
        return  list;
    }

    @Override
    public Boolean deleteForTime(Date date) {
        String sql ="delete from TaskChart where uptime <=:uptime";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("uptime",date);
        namedParameterJdbcTemplate.update(sql, parameters);
        return true;
    }
}
