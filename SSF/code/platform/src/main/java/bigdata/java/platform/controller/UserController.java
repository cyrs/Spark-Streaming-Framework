package bigdata.java.platform.controller;

import bigdata.java.platform.beans.PageBean;
import bigdata.java.platform.beans.Resoult;
import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.core.Permission;
import bigdata.java.platform.service.UserService;
import bigdata.java.platform.util.Comm;
import org.apache.catalina.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping()
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "login")
    public ModelAndView getLogin()
    {
        ModelAndView mv = new ModelAndView();
//        mv.addObject("username","admin");
        mv.setViewName("login");
        return mv;
    }

    @PostMapping(value = "login")
    public ModelAndView doLogin(String login_name, String login_pass, HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView();
        if(StringUtils.isBlank(login_name) || StringUtils.isBlank(login_pass))
        {
            mv.addObject("username",login_name);
            mv.addObject("error","用户名或者密码不能为空");
            mv.setViewName("login");
            return mv;
        }

        UserInfo userInfo = userService.doLogin(login_name, login_pass);
        if(userInfo ==null)
        {//登录失败
            mv.addObject("username",login_name);
            mv.addObject("error","用户名或者密码错误");
            mv.setViewName("login");
        }
        else
        {//登录成功
            request.getSession().setAttribute("userinfo",userInfo);
            mv.setViewName("redirect:/main");
        }
        return mv;
    }

    @GetMapping(value = "logout")
    public ModelAndView getLoginOut(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView();
        request.getSession().removeAttribute("userinfo");
        mv.setViewName("redirect:/login");
        return mv;
    }

    @GetMapping(value = "user/list")
    @Permission(Comm.ISADMIN)
    public ModelAndView getList(@RequestParam(value = "page",defaultValue = "1")Integer page, String nickName, String userName)
    {
        ModelAndView view = new ModelAndView();
        List<UserInfo> list= userService.list();

        List<UserInfo> filterlist=new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            UserInfo userInfo = list.get(i);
            Boolean nickNameFiltered = true;
            Boolean userNameFiltered = true;

            if(!StringUtils.isBlank(nickName))
            {
                nickNameFiltered = userInfo.getNickName().contains(nickName.trim());
            }
            if(!StringUtils.isBlank(userName))
            {
                userNameFiltered = userInfo.getUserName().contains(userName.trim());
            }

            if(nickNameFiltered&&userNameFiltered)
            {
                filterlist.add(userInfo);
            }
        }
        PageBean<UserInfo> pageBean = new PageBean<>(filterlist,filterlist.size(),page);
        view.addObject("list",pageBean);
        view.setViewName("userlist");
        return view;
    }

    @GetMapping(value = "user/edit")
    @Permission(Comm.ISADMIN)
    public ModelAndView getEdit(Integer userId)
    {
        ModelAndView view = new ModelAndView();
        UserInfo userInfo = new UserInfo();
        if(userId!=null)
        {//修改
            userInfo = userService.get(userId);
        }
        view.addObject("userInfo",userInfo);
        view.setViewName("useredit");
        return view;
    }

    @PostMapping(value = "user/edit")
    @Permission(Comm.ISADMIN)
    public ModelAndView doEdit(UserInfo userInfo)
    {
        ModelAndView view = new ModelAndView();
        Boolean chk = true;
        if(StringUtils.isBlank(userInfo.getNickName()))
        {
            view.addObject("nickNameErr","用户姓名不能为空！");
            chk=false;
        }

        if(StringUtils.isBlank(userInfo.getPassWord()))
        {
            view.addObject("passWordErr","密码不能为空！");
            chk=false;
        }


        if(StringUtils.isBlank(userInfo.getQueueName()))
        {
            view.addObject("queueNameErr","yarn队列不能为空！");
            chk=false;
        }

        if(StringUtils.isBlank(userInfo.getWarnPhone()))
        {
            view.addObject("warnPhoneErr","告警手机不能为空！");
            chk=false;
        }

        if(userInfo.getIsAdmin() ==null)
        {
            userInfo.setIsAdmin("0");
        }
        else
        {
            userInfo.setIsAdmin("1");
        }

        if(userInfo.getUserId() ==null)
        {//新增
            if(StringUtils.isBlank(userInfo.getUserName()))
            {
                view.addObject("userNameErr","用户账号不能为空！");
                chk=false;
            }
            if(chk)
            {
                List<UserInfo> list = userService.list();
                Boolean find= false;
                for (int i = 0; i < list.size(); i++) {
                    UserInfo u = list.get(i);
                    if(u.getUserName().trim().toLowerCase().equals(userInfo.getUserName().trim().toLowerCase()))
                    {
                        find=true;
                        break;
                    }
                }
                if(find)
                {
                    view.addObject("userNameErr","用户账号"+userInfo.getUserName()+"已存在，请修改！");
                    view.addObject("userInfo",userInfo);
                    view.setViewName("useredit");
                }
                else
                {
                    userService.add(userInfo);
                    view.setViewName("redirect:/user/list");
                }
            }
            else
            {
                view.addObject("userInfo",userInfo);
                view.setViewName("useredit");
            }
        }
        else
        {//修改
            if(chk)
            {
                userService.edit(userInfo);
                view.setViewName("redirect:/user/list");

            }
            else
            {
                UserInfo u = userService.get(userInfo.getUserId());
                userInfo.setUserName(u.getUserName());
                view.setViewName("useredit");
                view.addObject("userInfo",userInfo);
            }
        }

//        userService.edit(userInfo);

        return view;
    }

    @PostMapping("user/delete")
    @Permission(Comm.ISADMIN)
    @ResponseBody
    public Resoult doDelete(Integer userId)
    {
        userService.delete(userId);
        return Resoult.success();
    }
}
