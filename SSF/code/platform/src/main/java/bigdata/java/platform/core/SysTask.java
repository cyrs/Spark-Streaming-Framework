package bigdata.java.platform.core;

import bigdata.java.platform.service.TaskChartService;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.util.DateUtil;
import bigdata.java.platform.util.SpringContextUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Date;

@DisallowConcurrentExecution
public class SysTask extends QuartzJobBean {

    @Autowired
    TaskService taskService;
    @Autowired
    TaskChartService taskChartService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        synchronized (ScheduleLock.class) {
            try {
                //所有任务的重试次数 归0
                taskService.clearAllTaskErrCountAndClearTryCount();
                ScheduledTask.taskInfoLog.info("执行错误次数，重启次数清0");

                //删除7天前的数据
                Date date = new Date();
                Date date7 = DateUtil.addDay(date, -7);
                taskChartService.deleteForTime(date7);
                ScheduledTask.taskInfoLog.info("TaskChart删除7天前的数据");
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }

    }
}
