package bigdata.java.platform.util;

import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.TaskLog;
import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.core.LoggerBuilder;
import bigdata.java.platform.core.ScheduledTask;
import bigdata.java.platform.service.TaskLogService;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

@Service
public class Comm {

    public static Map<Integer,String> userQuaueNameMap = new ConcurrentHashMap<>();

    public static final String ISADMIN="IsAdmin";
    static final LoggerBuilder loggerBuilder = new LoggerBuilder();
    static final ch.qos.logback.classic.Logger sendMsgLog = loggerBuilder.getLogger("sendmsg");

    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }
    public static String executeLinuxCmd(String cmd) {
        System.out.println("got cmd job : " + cmd);
        Runtime run = Runtime.getRuntime();
        try {
            Process process = run.exec(cmd);
            InputStream in = process.getInputStream();
            BufferedReader bs = new BufferedReader(new InputStreamReader(in));
            // System.out.println("[check] now size \n"+bs.readLine());
                StringBuffer out = new StringBuffer();
                byte[] b = new byte[8192];
                for (int n; (n = in.read(b)) != -1;) {
                out.append(new String(b, 0, n));
            }
            System.out.println("job result [" + out.toString() + "]");
            in.close();
            // process.waitFor();
            process.destroy();
            return out.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStackTrace(Exception e)
    {
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        e.printStackTrace(new PrintWriter(buff,true));
        String msg = buff.toString();
        try
        {
            buff.close();
        }
        catch (Exception ex)
        {
            e.printStackTrace();
        }
        return msg;
    }
    public static String httpGet(String url)
    {
        return httpGet(url,null);
    }
    public static void closeHttpConnection(CloseableHttpClient httpClient,HttpResponse response)
    {
        if(response!=null)
        {
            try {
                ((CloseableHttpResponse) response).close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(httpClient!=null)
        {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String httpGet(String url , Map<String,String> header)
    {
        CloseableHttpClient httpClient = null;
        HttpGet get =null;
        HttpResponse response=null;
        String resoult=null;
        try{
//            httpClient = HttpClientBuilder.create().build();
            RequestConfig requestConfig = RequestConfig.custom().setExpectContinueEnabled(false).build();
//            httpClient.getParams().setBooleanParameter("http.protocol.expect-continue",false);
            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
            get = new HttpGet(url);
            get.setConfig(requestConfig);
            if(header !=null)
            {
                for (Map.Entry<String,String> entry : header.entrySet()) {
                    get.addHeader(entry.getKey(),entry.getValue());
                }
            }
            get.addHeader("Connection","close");
            response = httpClient.execute(get);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                    || response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
            {
                resoult = EntityUtils.toString(response.getEntity());
                return resoult;
            }
        }
        catch (Exception e)
        {
            throw  new RuntimeException(e);
        }
        finally {
            closeHttpConnection(httpClient,response);
        }
        return resoult;
    }

    public static String httpDelete(String url , Map<String,String> header)
    {
        CloseableHttpClient httpClient = null;
        HttpResponse response = null;
        HttpDelete delete = null;
        String resoult=null;
        try{
//            httpClient = HttpClientBuilder.create().build();
            RequestConfig requestConfig = RequestConfig.custom().setExpectContinueEnabled(false).build();
            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
            delete = new HttpDelete(url);
            delete.setConfig(requestConfig);
            for (Map.Entry<String,String> entry : header.entrySet()) {
                delete.addHeader(entry.getKey(),entry.getValue());
            }
            delete.addHeader("Connection","close");
            response = httpClient.execute(delete);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                    || response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
            {
                resoult = EntityUtils.toString(response.getEntity());
                return resoult;
            }
        }
        catch (Exception e)
        {
            throw  new RuntimeException(e);
        }
        finally {
            closeHttpConnection(httpClient,response);
        }
        return resoult;
    }

    public static String httpPostJson(String url , Map<String,String> header, String json)
    {
        CloseableHttpClient httpClient = null;
        HttpResponse response =null;
        HttpPost post =null;
        String resoult=null;
        try{
//            httpClient = HttpClientBuilder.create().build();
            RequestConfig requestConfig = RequestConfig.custom().setExpectContinueEnabled(false).build();
            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
            post = new HttpPost(url);
            post.setConfig(requestConfig);
            StringEntity entity = new StringEntity(json);
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            for (Map.Entry<String,String> entry : header.entrySet()) {
                post.addHeader(entry.getKey(),entry.getValue());
            }
            post.addHeader("Connection","close");
            post.setEntity(entity);
            response = httpClient.execute(post);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                    || response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
            {
                resoult = EntityUtils.toString(response.getEntity());
                return resoult;
            }
        }
        catch (Exception e)
        {
            throw  new RuntimeException(e);
        }
        finally {
            closeHttpConnection(httpClient,response);
        }
        return resoult;
    }


    public static String execCurl(String[] cmds)
    {
        ProcessBuilder process = new ProcessBuilder(cmds);
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            return builder.toString();
        } catch (Exception e) {
            System.out.print("error");
            e.printStackTrace();
        }
        return null;
    }

    public static String taskFiles(String sparkApplication, String type,TTask tTask)
    {
        String jarsString="";
        String jars = "";
        if("jars".equals(type))
        {
            jars = tTask.getJars();
        }
        else if("files".equals(type))
        {
            jars = tTask.getFiles();
        }


        if(!org.apache.commons.lang3.StringUtils.isBlank(jars))
        {
            String[] split = jars.split(",");
            for (int i = 0; i < split.length; i++) {
                if(!org.apache.commons.lang3.StringUtils.isBlank(split[i]))
                {
                    jarsString +=  "\""+sparkApplication+"/"+type+"/" +tTask.getUserId() + "/" +split[i]+"\",";
                }
            }
            if(!org.apache.commons.lang3.StringUtils.isBlank(jarsString))
            {
                jarsString = jarsString.substring(0,jarsString.length() - 1);
            }
        }

        return jarsString;
    }

    public static String taskFiles(String sparkApplication, String type,String files)
    {
        String jarsString="";
        String jars = files;
        if(!org.apache.commons.lang3.StringUtils.isBlank(jars))
        {
            String[] split = jars.split(",");
            for (int i = 0; i < split.length; i++) {
                if(!org.apache.commons.lang3.StringUtils.isBlank(split[i]))
                {
                    jarsString +=  "\""+sparkApplication+"/"+type+"/"+split[i]+"\",";
                }
            }
            if(!org.apache.commons.lang3.StringUtils.isBlank(jarsString))
            {
                jarsString = jarsString.substring(0,jarsString.length() - 1);
            }
        }

        return jarsString;
    }

    public static Boolean FileExists(String filePath)
    {
        File targetFile = new File(filePath);
        if(targetFile.exists()){
            return true;
        }
        else
        {
            return false;
        }
    }
    public static Boolean chkParams(String args)
    {
        String[] split = args.split(",\"");
        for (int i = 0; i < split.length; i++) {
            String sp = split[i];
            String[] kv = sp.split(":");

            if(kv.length > 2)
            {
                kv = sp.split("\":");
            }

            if(kv.length !=2)
            {
                return false;
            }
            else
            {
                String s = kv[0];
                if(StringUtils.isBlank(s) || s.equals("\"")|| s.equals("\"\"")  )
                {
                    return false;
                }
                s = kv[1];
                if(StringUtils.isBlank(s) || s.equals("\"")|| s.equals("\"\"")  )
                {
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * 字符串转换为Ascii
     * @param value
     * @return
     */
    public static String stringToAscii(String value)
    {
        StringBuffer sbu = new StringBuffer();
        char[] chars = value.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if(i != chars.length - 1)
            {
                sbu.append((int)chars[i]);
            }
            else {
                sbu.append((int)chars[i]);
            }
        }
        return sbu.toString();
    }

    /**判断是否整数
     * @param string  字符串
     *
     */
    public static boolean isInteger(String string)
    {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(string).matches();

    }


    public static String uploadFile(byte[] file, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        String fullPath = filePath+fileName;
        FileOutputStream out = new FileOutputStream(fullPath);
        out.write(file);
        out.flush();
        out.close();
        return fullPath;
    }

    //解析thymeleaf模板，转换string
    public static String Resolver2String(String htmltemplatefile ,Context context)
    {
        //构造模板引擎
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setPrefix("templates");//模板所在目录，相对于当前classloader的classpath。
        resolver.setSuffix(".html");//模板文件后缀
        resolver.setCacheable(false);
        resolver.setCharacterEncoding("utf-8");
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);
        //渲染模板
        String content= templateEngine.process(htmltemplatefile, context);
        return content;
    }

    public static boolean deleteFile(String sPath) {
        File file = new File(sPath);
        if (file.isFile() && file.exists()) {
            file.delete();
            return true;
        }
        return  false;
    }

    public static boolean checkAuthority(String key)
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if(request==null)return false;
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userinfo");
        if(userInfo!=null)
        {
            if(!userInfo.getIsAdmin().equals("1"))
            {
                return false;
            }
            return true;
        }
        return false;
    }

    public static UserInfo getCurrentUserInfo()
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userinfo");
        return userInfo;
    }

    public static Integer getUserId()
    {
        return getCurrentUserInfo().getUserId();
    }

    public static Boolean allowStatus(Integer currentStatus, Integer updateStatus)
    {
        if(currentStatus.intValue() == TTask.STARTED && updateStatus.intValue() == TTask.STOPING)
        {
            return true;
        }
        if(currentStatus.intValue() == TTask.STOPED && updateStatus.intValue() == TTask.STARTING)
        {
            return true;
        }
        if(currentStatus.intValue() == TTask.STOPED && updateStatus.intValue() == TTask.DELETE)
        {
            return true;
        }
        return false;
    }

    public static boolean ExecYarnLogs(String applicationId,String yarnAccount) throws IOException, InterruptedException {
        String shpath ="/home/work/xtb/buildsparklog.sh";
        Process ps = Runtime.getRuntime().exec(shpath,new String[]{applicationId,yarnAccount});
        ps.waitFor();
        return true;
    }

    public static boolean ExecYarnLogs2(String applicationId,String yarnAccount) throws IOException, InterruptedException {

        ProcessBuilder pb = new ProcessBuilder("./buildsparklog.sh",applicationId,yarnAccount);
        pb.directory(new File(LivyUtil.getSysSet().getShellScript() + "/"));
        String s = null;
        int runningStatus = 0;
        try
        {
            Process p = pb.start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((s = stdInput.readLine()) != null)
            {
                System.out.println(s);
            }
            while ((s = stdError.readLine()) != null)
            {
                System.out.println(s);
            }
            runningStatus = p.waitFor();
        }
        catch (Exception e)
        {
            new RuntimeException(e);
        }

        return true;
    }

    public static Boolean hasDataPermission(Integer userId)
    {
        if(Comm.getCurrentUserInfo().getIsAdmin().equals("1"))
        {
             return true;
        }
        else if(userId.intValue() == Comm.getCurrentUserInfo().getUserId().intValue())
        {
            return true;
        }
        return false;
    }

    public static Integer getUserIdForAES(String apptoken)
    {
        String decrypt = AESUtils.decrypt(apptoken);
        String s = decrypt.split("-")[0];
        if(s.equals("doam"))
        {
            return Integer.parseInt(decrypt.split("-")[1]);
        }
        return null;
    }

    public static void sendWarnMsg(String warnPhone,String content)
    {
        String[] split = warnPhone.split(",");
        for (int i = 0; i < split.length; i++) {
            String mobile = split[i];
            if(!StringUtils.isBlank(mobile))
            {
                sendMsg(mobile,content);
                sendMsgLog.info("mobile="+mobile + ",content="+content);
            }
        }
    }

    public static Boolean kill(TTask tTask, SysSet sysSet, TaskLogService taskLogService)
    {
        Map<String,String> header = new HashMap<>();
        header.put("X-Requested-By","user");
        TaskLog taskLog;
        String resoult;
        Boolean succees=false;
        try
        {
            taskLog =tTask.getTaskLog();
            resoult = Comm.httpDelete(sysSet.getLivy() + "/batches/" + taskLog.getBatchId(), header);
            if(resoult==null || resoult.equals("{\"msg\":\"deleted\"}"))
            {
                taskLogService.stop(tTask.getTaskId(),taskLog.getTaskLogId(),taskLog.getBatchId());
                Integer taskId = null;
                if(tTask!=null)
                {
                    taskId = tTask.getTaskId();
                }
                Integer taskLogId = null;
                Integer id = null;
                if(taskLog!=null)
                {
                    taskLogId = taskLog.getTaskLogId();
                    id = taskLog.getBatchId();
                }
                ScheduledTask.JiYa.remove(tTask.getTaskId());
                ScheduledTask.taskInfoLog.info("livy kill success taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + id );
                succees=true;
            }
        }
        catch (Exception e)
        {
            ScheduledTask.taskErrLog.error("livy kill fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+e.getMessage()+"\nStackTarce: "+Comm.getStackTrace(e));
        }
        return succees;
    }


    public static String sendMsg(String mobile,String content)
    {
        String resoult=null;
        CloseableHttpClient httpClient = null;
        HttpPost post = null;
        HttpResponse response =null;
        try
        {
            String requestBody="<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                    "<soap:Body>" +
                    "<send>" +
                    "<loginName>sms_check</loginName>" +
                    "<password>site..0603</password>" +
                    "<channelID>0000000047</channelID>" +
                    "<mobile>" + mobile +"</mobile>" +
                    "<content>"+content+"</content>" +
                    "<type>1</type>" +
                    "</send>" +
                    "</soap:Body>" +
                    "</soap:Envelope>";
            String url="http://sms.hn.sgcc.com.cn/services/SmsClient?wsdl";
//            httpClient = HttpClientBuilder.create().build();
            RequestConfig requestConfig = RequestConfig.custom().setExpectContinueEnabled(false).build();
            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
            post = new HttpPost(url);
            post.setConfig(requestConfig);
            StringEntity entity = new StringEntity(requestBody, Charset.forName("UTF-8"));
            post.addHeader("SOAPAction","http://sms.hn.sgcc.com.cn/services/SmsClient");
            post.addHeader("Connection","close");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("text/xml");
            post.setEntity(entity);
            response = httpClient.execute(post);
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK
                    || response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
            {
                resoult = EntityUtils.toString(response.getEntity());
                return resoult;
            }
            else
            {
                throw  new RuntimeException(resoult);
            }
        }
        catch (Exception e)
        {
            throw  new RuntimeException(e);
        }
        finally {
            closeHttpConnection(httpClient,response);
        }
    }
}
